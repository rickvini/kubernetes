apiVersion: "vault.banzaicloud.com/v1alpha1"
kind: "Vault"
metadata:
  name: "vault"
spec:
  size: 3
  image: vault:1.2.2
  bankVaultsImage: banzaicloud/bank-vaults:0.5.3 # Specify the ServiceAccount where the Vault Pod and the Bank-Vaults configurer/unsealer is running
  serviceAccount: vault # Specify how many nodes you would like to have in your etcd cluster
  serviceType: ClusterIP

  ingress:
    annotations:
      ingress.kubernetes.io/ssl-redirect: "true"
      kubernetes.io/tls-acme: "true"
      kubernetes.io/ingress.class: nginx
      certmanager.k8s.io/cluster-issuer: "letsencrypt-production"
      nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
    spec:
      tls:
      - hosts:
        - vault.corp.justin-tech.com
      rules:
      - host: vault.corp.justin-tech.com
        http:
          paths:
          - backend:
              serviceName: vault
              servicePort: 8200

  statsdDisabled: false
  # NOTE: -1 disables automatic etcd provisioning
  etcdSize: -1 
  # Support for distributing the generated CA certificate Secret to other namespaces.
  # Define a list of namespaces or use ["*"] for all namespaces.
  caNamespaces:
    - "*" # Describe where you would like to store the Vault unseal keys and root token.
  unsealConfig:
    kubernetes:
      secretNamespace: vault-infra # A YAML representation of a final vault config file.
  # See https://www.vaultproject.io/docs/configuration/ for more information.
  config:
    storage:
      consul:
        address: consul-consul-server.consul:8500
        path: vault/
        scheme: http
        token: changeme
    listener:
      tcp:
        address: "0.0.0.0:8200"
        tls_cert_file: /vault/tls/server.crt
        tls_key_file: /vault/tls/server.key
    api_addr: https://127.0.0.1:8200
    telemetry:
      statsd_address: localhost:9125
      prometheus_retention_time: "24h"
      disable_hostname: true
    ui: true

  serviceMonitorEnabled: false

  externalConfig:
    policies:
      - name: prometheus-operator
        rules: path "sys/metrics" {
            capabilities = ["list", "read"]
            }
      - name: allow_secrets
        rules: path "secret/*" {
          capabilities = ["create", "read", "update", "delete", "list"]
          }

    auth:
      - type: kubernetes
        roles:
          - name: prometheus
            bound_service_account_names: prometheus
            bound_service_account_namespaces: ["monitoring", "vault-infra"]
            policies: prometheus-operator
            ttl: 5h
          - name: default
            bound_service_account_names: ["default", "vault-secrets-webhook"]
            #bound_service_account_names: "*"
            #bound_service_account_namespaces: ["vault-infra", "vswh", "default"]
            bound_service_account_namespaces: "*"
            policies: ["allow_secrets"]
            ttl: 1h

    secrets:
      - path: secret
        type: kv
        description: General secrets.
        options:
          version: 2

---

apiVersion: v1
kind: ConfigMap
metadata:
  name: vault-agent-config
data:
  vault-agent-config.hcl: |
    auto_auth {
      method "kubernetes" {
        mount_path = "auth/kubernetes"
        config = {
          role = "prometheus"
        }
      }
      sink "file" {
        config = {
          path = "/home/vault/config-out/.vault-token"
        }
      }
    }

---

apiVersion: v1
kind: ServiceAccount
metadata:
  name: prometheus

---

apiVersion: monitoring.coreos.com/v1
kind: Prometheus
metadata:
  name: prometheus
spec:
  serviceAccountName: prometheus
  serviceMonitorSelector:
    matchLabels:
      app.kubernetes.io/name: vault
  configMaps:
  - vault-agent-config
  containers:
  - name: vault-agent-auth
    image: vault
    securityContext:
      runAsUser: 65534
    volumeMounts:
      - name: configmap-vault-agent-config
        mountPath: /etc/vault
      - name: config-out
        mountPath: /home/vault/config-out
    env:
      - name: VAULT_ADDR
        value: https://vault.vault-infra:8200
      - name: VAULT_SKIP_VERIFY
        value: "true"
    command: ["vault"]
    args:
      [
        "agent",
        "-config=/etc/vault/vault-agent-config.hcl",
        "-log-level=debug",
      ]

---

apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: vault-prometheus
  namepsace: vault-infra
spec:
  selector:
    matchLabels:
      app: vault
  namespaceSelector:
    matchNames:
      - vault-infra
  endpoints:
    - interval: 30s
      path: /metrics
      params:
        format: 
          - prometheus
      port: metrics
      scheme: http
      tlsConfig:
        insecureSkipVerify: true
      scrapeTimeout: 30s
      bearerToken: 'vault:secret/tokens/vault-prometheus#TOKEN'
